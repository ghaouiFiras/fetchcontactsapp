package com.firas.fetchcontacts.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.firas.fetchcontacts.data.dao.ContactDAO
import com.firas.fetchcontacts.model.Contact

@Database(entities = [Contact::class], version = 2)
abstract class AppDataBase : RoomDatabase() {
    abstract fun contactDAO(): ContactDAO

    companion object {
        // Singleton prevents multiple
        // instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: AppDataBase? = null

        fun getDatabase(context: Context): AppDataBase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDataBase::class.java,
                    "contacts_database"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}