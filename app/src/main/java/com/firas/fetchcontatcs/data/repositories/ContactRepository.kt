package com.firas.fetchcontacts.data.repositories

import android.database.Cursor
import android.provider.ContactsContract
import com.firas.fetchcontacts.data.database.AppDataBase
import com.firas.fetchcontacts.model.Contact
import kotlinx.coroutines.flow.Flow

class ContactRepository(val appDb: AppDataBase) {

    private  fun insertListContacts(contactsListToInsert: List<Contact>) {
        appDb.contactDAO().insertListContacts(contactsListToInsert)
    }

     fun getAllContacts(): Flow<List<Contact>> {
        return appDb.contactDAO().getAllContacts()
    }


     fun fetchContactsFromDevice(cursor: Cursor) {
        /* val contentResolver = mApplication.contentResolver
         val cursor =
             contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)*/
        val fetchedContactList = mutableListOf<Contact>()

        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
               /* val phoneNumber =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))*/


               val contact = Contact(id, name)
               // fetchedContactList.add(contact)

                    fetchedContactList.add(contact)


              /* if (phoneNumber.toInt() > 0) {
                      val pCursor = contentResolver.query(
                          ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                          null,
                          ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                          arrayOf(id),
                          null
                      )

                      if (pCursor != null) {
                          while (pCursor.moveToNext()) {

                          }
                          pCursor.close()
                      }
                  }*/
            }

            cursor.close()
        }

        insertListContacts(fetchedContactList)
    }

}

