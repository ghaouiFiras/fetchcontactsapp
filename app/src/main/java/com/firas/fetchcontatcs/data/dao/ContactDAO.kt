package com.firas.fetchcontacts.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.firas.fetchcontacts.model.Contact
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactDAO {
    @Query("SELECT  * FROM contact order by name ")
    fun getAllContacts(): Flow<List<Contact>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertListContacts(contactsList: List<Contact>)


}