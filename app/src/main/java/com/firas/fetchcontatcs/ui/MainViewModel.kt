package com.firas.fetchcontacts.ui

import android.app.Application
import android.provider.ContactsContract
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.firas.fetchcontacts.data.repositories.ContactRepository
import com.firas.fetchcontacts.model.States
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainViewModel(
    application: Application, private val contactRepository: ContactRepository,
) : AndroidViewModel(application) {

    var result: MutableStateFlow<States> = MutableStateFlow(States.onLoad)


    fun init() {
        val cursor = createCursor()
        viewModelScope.launch(Dispatchers.IO) {
            contactRepository.fetchContactsFromDevice(cursor)
        }
        fetchContact()
    }

    private fun createCursor() = getApplication<Application>().contentResolver.query(
        ContactsContract.Contacts.CONTENT_URI,
        null,
        null,
        null,
        null
    )!!


    fun fetchContact() = viewModelScope.launch {
        contactRepository.getAllContacts().collectLatest {
            result.emit(States.onLoad)
            if (!it.isEmpty())
                result.emit(States.onSuccess(it))
            else
                result.emit(States.onError(" oops !! no Data "))
        }
    }


}