package com.firas.fetchcontatcs.ui

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.firas.fetchcontacts.data.repositories.ContactRepository
import com.firas.fetchcontacts.ui.MainViewModel

class MainViewModelFactory (val application: Application,val contactRepository: ContactRepository) :ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return modelClass.getConstructor(Application::class.java,ContactRepository::class.java).newInstance(application,contactRepository)
        } else {
            throw IllegalArgumentException("something wrong unable to create instance ")
        }    }
}