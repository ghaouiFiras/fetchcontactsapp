package com.firas.fetchcontacts.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.firas.fetchcontacts.model.Contact
import com.firas.fetchcontatcs.R
import java.util.*
import kotlin.collections.ArrayList

class ContatcsAdapter(private val context: Context) :
    RecyclerView.Adapter<ContatcsAdapter.ViewHolder>(),Filterable {
    private var contactList: List<Contact> = ArrayList()
    private var filteredContactList: List<Contact> = ArrayList()


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvname = itemView.findViewById<TextView>(R.id.nameContact)

        fun bind(contact: Contact) {
            tvname.text = contact.name
            itemView.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                val uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, contact.id)
                intent.data = uri
                context.startActivity(intent)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_contact, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(filteredContactList.get(position))
    }

    override fun getItemCount(): Int {
        return filteredContactList.size
    }

    fun setData(contactListToset: List<Contact>) {
        contactList = contactListToset
        filteredContactList = contactList
        notifyDataSetChanged()
    }

   override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint?.toString() ?: ""
                if (charString.isEmpty()) filteredContactList = contactList else {
                    val filteredList = ArrayList<Contact>()
                    contactList
                        .filter {
                        (it.name.contains(constraint!!,ignoreCase = true))
                        }
                        .forEach { filteredList.add(it) }
                    filteredContactList = filteredList

                }
                return FilterResults().apply { values = filteredContactList }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredContactList = if (results?.values == null)
                    ArrayList()
                else
                    results.values as ArrayList<Contact>
                notifyDataSetChanged()
            }

        }
    }
}