package com.firas.fetchcontacts.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.firas.fetchcontacts.data.database.AppDataBase
import com.firas.fetchcontacts.data.repositories.ContactRepository
import com.firas.fetchcontacts.model.States
import com.firas.fetchcontatcs.databinding.ActivityMainBinding
import com.firas.fetchcontatcs.ui.MainViewModelFactory
import kotlinx.coroutines.flow.collectLatest

class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var contactListAdapter: ContatcsAdapter

    private val CONTACTS_READ_REQ_CODE = 100


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.searchEd.setOnQueryTextListener(this)

        val db = AppDataBase.getDatabase(applicationContext)
        val contactRepository = ContactRepository(db)
        val viewModelFactory = MainViewModelFactory(application, contactRepository)
        mainViewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

        initRecyclerView()
        loadContacts()
        requestPermission()

    }


    private fun initRecyclerView() {
        contactListAdapter = ContatcsAdapter(this)
        binding.contactsRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(this@MainActivity)
            this.adapter = contactListAdapter
            this.setHasFixedSize(true)
            this.adapter?.notifyDataSetChanged()
            this.addItemDecoration(
                DividerItemDecoration(
                    baseContext,
                    LinearLayoutManager.VERTICAL
                )
            )
        }
    }

    private fun loadContacts() {
        lifecycleScope.launchWhenCreated {
            mainViewModel.result.collectLatest { st ->
                when (st) {
                    is States.onSuccess -> {
                        binding.progressBar.visibility = View.GONE
                        binding.txtvError.visibility = View.GONE
                        binding.contactsRecyclerView.visibility = View.VISIBLE
                        binding.searchEd.visibility = View.VISIBLE
                        contactListAdapter.setData(st.data)


                    }
                    is States.onLoad -> {
                        binding.progressBar.visibility = View.VISIBLE
                        binding.txtvError.visibility = View.GONE
                        binding.contactsRecyclerView.visibility = View.GONE
                        binding.searchEd.visibility = View.GONE


                    }
                    is States.onError -> {
                        binding.progressBar.visibility = View.GONE
                        binding.txtvError.visibility = View.VISIBLE
                        binding.txtvError.text = st.errorMsg
                        binding.contactsRecyclerView.visibility = View.GONE
                        binding.searchEd.visibility = View.GONE


                    }
                }

            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == CONTACTS_READ_REQ_CODE
        ) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                mainViewModel.init()

            }
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            &&
            checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.READ_CONTACTS),
                CONTACTS_READ_REQ_CODE
            )
        } else {

            mainViewModel.init()
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        contactListAdapter.filter.filter(newText)
        return true
    }


}