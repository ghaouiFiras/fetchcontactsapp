package com.firas.fetchcontacts.model

sealed class States {
    class onSuccess(val data: List<Contact>) : States()
    class onError(val errorMsg: String) : States()
    object onLoad : States()
}