package com.firas.fetchcontacts.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contact")
data class Contact(
    @NonNull
    @PrimaryKey
    val id: String,
    val name: String,
)